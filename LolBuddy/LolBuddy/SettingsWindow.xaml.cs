﻿using LolBuddy.Classes;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LolBuddy
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : MetroWindow {
        public SettingsWindow() {
            InitializeComponent();

            txtInstallDir.Text = Properties.Settings.Default.InstallDir;
            CheckInstallDir(txtInstallDir.Text);
        }

        private bool CheckInstallDir(string path) {
            bool isValidInstallDir = LocalClient.CheckInstallDir(path);
            if(isValidInstallDir) {
                chkValidInstallDir.IsChecked = true;
                chkValidInstallDir.Content = "Found League of Legends client!";
                return true;
            }

            chkValidInstallDir.IsChecked = false;
            chkValidInstallDir.Content = "Could not find League of Legends client!";
            return false;
        }

        private void BtnBrowsePath_Click(object sender, RoutedEventArgs e) {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            DialogResult dialogResult = folderBrowserDialog.ShowDialog();
            if (dialogResult == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(folderBrowserDialog.SelectedPath)) {
                bool isValidInstallDir = CheckInstallDir(folderBrowserDialog.SelectedPath);
                if(isValidInstallDir) {
                    Properties.Settings.Default.InstallDir = folderBrowserDialog.SelectedPath;
                }
                txtInstallDir.Text = folderBrowserDialog.SelectedPath;
            }
        }
    }
}
