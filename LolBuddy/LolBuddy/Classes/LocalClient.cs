﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace LolBuddy.Classes {
    class LocalClient {
        public static string apiUrl;
        public static string apiToken;

        public LocalClient() {
            // Check if the lockfile exists
            string lockfilePath = $"{Properties.Settings.Default.InstallDir}/lockfile";
            if (!File.Exists(lockfilePath)) {
                return;
            }

            string[] lockfileData;
            using (var fileStream = new FileStream(lockfilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (var textReader = new StreamReader(fileStream)) {
                lockfileData = textReader.ReadToEnd().Split(':');
            }

            apiUrl = $"{lockfileData[4]}://127.0.0.1:{lockfileData[2]}";
            apiToken = Convert.ToBase64String(Encoding.UTF8.GetBytes($"riot:{lockfileData[3]}")); ;
        }

        public static bool CheckInstallDir(string path) {
            if (File.Exists($"{path}/LeagueClient.exe")) {
                return true;
            }

            return false;
        }

        public string GetClientJson(string endpoint) {
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);

            // Create a new WebRequest
            WebRequest webRequest = WebRequest.Create($"{apiUrl}{endpoint}");

            // Set some headers
            webRequest.Headers["ContentType"] = "application/json";
            webRequest.Headers["Authorization"] = "Basic " + apiToken;


            // Get the HttpWebWesponse
            HttpWebResponse httpWebResponse = (HttpWebResponse)webRequest.GetResponse();

            // Get the response stream from the HttpWebResponse
            Stream responseStream = httpWebResponse.GetResponseStream();

            // Read the response stream into something workable
            StreamReader streamReader = new StreamReader(responseStream);

            // Return the response stream
            return streamReader.ReadToEnd();
        }

        public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) {
            return true;
        }
    }
}
